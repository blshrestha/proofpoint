package com.company;

import org.junit.Before;
import org.junit.Test;

import java.time.Month;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class Tests {

    @Before
    public void setUp() {
        Services.addInventory(new RoomInfo(RoomType.ECONOMY,100, 2));
        Services.addInventory(new RoomInfo(RoomType.BASIC, 200, 3));
        Services.addInventory(new RoomInfo(RoomType.LUXURY, 300, 4));

//        Services.addReservation(new Reservation(RoomType.ECONOMY, new Date(), new Date()));
//        Services.addReservation(new Reservation(RoomType.ECONOMY, new Date(), new Date()));
//        Services.addReservation(new Reservation(RoomType.ECONOMY, new Date(), new Date()));
    }

    @Test
    public void testGetPriceNoPromotion() {
        assertEquals(100, Services.getPrice(RoomType.ECONOMY, new Date()));
    }

    @Test
    public void testUpdatePrice() {
        Services.updateInventoryPrice(RoomType.ECONOMY, 50);
        assertEquals(50, Services.getPrice(RoomType.ECONOMY, new Date()));
    }

    @Test
    public void testSpecialPrice() {
        // set Feb 1 - 27 promotion  ::: $10 only
        Calendar start = Calendar.getInstance();
        start.set(2018, Calendar.FEBRUARY, 1);

        Calendar end = Calendar.getInstance();
        end.set(2018, Calendar.FEBRUARY, 27);

        int specialPrice = 10;


        Services.setSpecialPrice(RoomType.ECONOMY, specialPrice, start.getTime(), end.getTime());


        Calendar dateWithinPromotion = Calendar.getInstance();
        dateWithinPromotion.set(2018, Calendar.FEBRUARY, 15);
        assertEquals("Rate during the promotion",10, Services.getPrice(RoomType.ECONOMY, dateWithinPromotion.getTime()));

        Calendar dateStartingPromotion = Calendar.getInstance();
        dateStartingPromotion.set(2018, Calendar.FEBRUARY, 15);
        assertEquals("Rate on start date of promotion",10, Services.getPrice(RoomType.ECONOMY, dateStartingPromotion.getTime()));

        Calendar dateEndingPromotion = Calendar.getInstance();
        dateEndingPromotion.set(2018, Calendar.FEBRUARY, 15);
        assertEquals("Rate on the end date of promotion",10, Services.getPrice(RoomType.ECONOMY, dateEndingPromotion.getTime()));

        Calendar dateBeforePromotion = Calendar.getInstance();
        dateBeforePromotion.set(2018, Calendar.JANUARY, 15);
        assertEquals("Rate before promotion", 100, Services.getPrice(RoomType.ECONOMY, dateBeforePromotion.getTime()));

        Calendar dateAfterPromotion = Calendar.getInstance();
        dateAfterPromotion.set(2018, Calendar.MARCH, 15);
        assertEquals("Rate after promotion", 100, Services.getPrice(RoomType.ECONOMY, dateAfterPromotion.getTime()));

    }

    @Test
    public void testAvailabilityWithFewerRoomsBooked() {

        Calendar reserveStart = Calendar.getInstance();
        reserveStart.set(2018, Calendar.JANUARY, 10);
        Calendar reserveEnd = Calendar.getInstance();
        reserveEnd.set(2018, Calendar.JANUARY, 20);

        Calendar checkStart = Calendar.getInstance();
        checkStart.set(2018, Calendar.JANUARY, 15);
        Calendar checkEnd = Calendar.getInstance();
        checkEnd.set(2018, Calendar.JANUARY, 16);


        Services.addReservation(new Reservation(RoomType.BASIC, reserveStart.getTime(), reserveEnd.getTime()));


        assertEquals("One of rooms available", true, Services.isAvailable(RoomType.BASIC, checkStart.getTime(), checkEnd.getTime()));
    }

    @Test
    public void testAvailabilityWithAllRoomsBooked() {
        //block all rooms for a period with reservations
        Calendar reserveStart = Calendar.getInstance();
        reserveStart.set(2018, Calendar.JANUARY, 10);
        Calendar reserveEnd = Calendar.getInstance();
        reserveEnd.set(2018, Calendar.JANUARY, 20);

        Services.addReservation(new Reservation(RoomType.ECONOMY, reserveStart.getTime(), reserveEnd.getTime()));
        Services.addReservation(new Reservation(RoomType.ECONOMY, reserveStart.getTime(), reserveEnd.getTime()));


        Calendar dateWithinBlocked = Calendar.getInstance();
        dateWithinBlocked.set(2018, Calendar.JANUARY, 11);
        Calendar dateWithinBlocked2 = Calendar.getInstance();
        dateWithinBlocked2.set(2018, Calendar.JANUARY, 12);
        Calendar dateBeforeBlocked = Calendar.getInstance();
        dateBeforeBlocked.set(2018, Calendar.JANUARY, 8);
        Calendar dateBeforeBlocked2 = Calendar.getInstance();
        dateBeforeBlocked2.set(2018, Calendar.JANUARY, 9);
        Calendar dateAfterBlocked = Calendar.getInstance();
        dateAfterBlocked.set(2018, Calendar.JANUARY, 25);
        Calendar dateAfterBlocked2 = Calendar.getInstance();
        dateAfterBlocked2.set(2018, Calendar.JANUARY, 27);

        assertEquals("Not available when start/end within blocked period",
                false, Services.isAvailable(RoomType.ECONOMY, dateWithinBlocked.getTime(), dateWithinBlocked2.getTime()));

        assertEquals("Not available when start within blocked period",
                false, Services.isAvailable(RoomType.ECONOMY, dateWithinBlocked.getTime(), dateAfterBlocked.getTime()));

        assertEquals("Not available when end within blocked period",
                false, Services.isAvailable(RoomType.ECONOMY, dateBeforeBlocked.getTime(), dateWithinBlocked.getTime()));

        assertEquals("Available when start/end before blocked period",
                true, Services.isAvailable(RoomType.ECONOMY, dateBeforeBlocked.getTime(), dateBeforeBlocked2.getTime()));

        assertEquals("Available when start/end after blocked period",
                true, Services.isAvailable(RoomType.ECONOMY, dateAfterBlocked.getTime(), dateAfterBlocked2.getTime()));


    }
}
