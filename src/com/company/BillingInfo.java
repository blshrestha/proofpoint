package com.company;

import java.util.Date;
import java.util.List;

public class BillingInfo {
    private String billedToName;
    private String billedToAddress;
    private Date billedDate;
    List<BillingItem> billingItems;

    public String getBilledToName() {
        return billedToName;
    }

    public void setBilledToName(String billedToName) {
        this.billedToName = billedToName;
    }

    public String getBilledToAddress() {
        return billedToAddress;
    }

    public void setBilledToAddress(String billedToAddress) {
        this.billedToAddress = billedToAddress;
    }

    public Date getBilledDate() {
        return billedDate;
    }

    public void setBilledDate(Date billedDate) {
        this.billedDate = billedDate;
    }

    public List<BillingItem> getBillingItems() {
        return billingItems;
    }

    public void setBillingItems(List<BillingItem> billingItems) {
        this.billingItems = billingItems;
    }

    public void addItem(BillingItem item) {
        billingItems.add(item);
    }
}
