package com.company;

import java.util.*;

public class Services {
    private static Map<RoomType, RoomInfo> inventory = new HashMap<RoomType, RoomInfo>();
    private static List<Reservation> reservations = new ArrayList<Reservation>();
    private static List<Promotion> promotions = new ArrayList<Promotion>();

    public static void addInventory(RoomInfo roomInfo) {
        inventory.put(roomInfo.getRoomType(), roomInfo);
    }

    public static void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }

    public static boolean isAvailable(RoomType type, Date start, Date end) {

        int inventoryCount = inventory.get(type).getCount();
        int reservationCount = 0;

        for(Reservation r : reservations) {
            if ( r.getRoomType().equals(type)
                    && ( equalOrBetween(start, r.getStartDate(), r.getEndDate())
                            || equalOrBetween(end,   r.getStartDate(), r.getEndDate()))
                ) {
                reservationCount++;
            }
        }

        return inventoryCount > reservationCount;
    }

    public static void setSpecialPrice(RoomType type, int specialPrice, Date from, Date to) {
        Promotion p = new Promotion();
        p.setRoomType(type);
        p.setSpecialPrice(specialPrice);
        p.setStartDate(from);
        p.setEndDate(to);

        promotions.add(p);
    }

    public static int getPrice(RoomType type, Date date) {
        int price = inventory.get(type).getPrice();

        for(Promotion p: promotions) {
            if (p.getRoomType().equals(type)
                    && equalOrBetween(date, p.getStartDate(), p.getEndDate())) {
                price = p.getSpecialPrice();
                break;
            }
        }
        return price;
    }

    public static void updateInventoryPrice(RoomType type, int newPrice) {
        inventory.get(type).setPrice(newPrice);
    }

    private static boolean equalOrBetween(Date date, Date start, Date end) {
        return date.equals(start)
                || date.equals(end)
                || (date.after(start) && date.before(end));
    }

}
