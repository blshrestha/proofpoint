package com.company;

public class RoomInfo {
    public RoomInfo(RoomType roomType, int price, int count) {
        this.roomType = roomType;
        this.price = price;
        this.count = count;
    }

    private RoomType roomType;
    private int price;
    private int count;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }
}
