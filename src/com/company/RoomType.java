package com.company;

public enum RoomType {
    ECONOMY, BASIC, LUXURY
}
